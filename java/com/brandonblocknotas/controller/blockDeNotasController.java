package com.brandonblocknotas.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.brandonblocknotas.service.NotasService;

@RestController
@RequestMapping("/api/blockDeNotas")
public class blockDeNotasController {

	@Autowired
	NotasService notasService;
	
	@GetMapping("/download")
	public ResponseEntity<String> downloadTextFileExample1()
			throws IOException, EncryptedDocumentException, InvalidFormatException {

//		XSSFWorkbook workbook = new XSSFWorkbook();
//		XSSFSheet sheetVariables = workbook.createSheet("variables");
//		cargarDatosvariables(sheetVariables);
//		try {
//			FileOutputStream outputStream = new FileOutputStream("src/main/resources/excelDatos.xlsx");
//			workbook.write(outputStream);
//			workbook.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		FileInputStream excelDatos = new FileInputStream(new File("src/main/resources/excelDatos.xlsx"));
		Workbook workbookExcelDatos = new XSSFWorkbook(excelDatos);
		Sheet hojaDeVariables = workbookExcelDatos.getSheetAt(0);
		String nombreTabla = extraerNombreTabla(hojaDeVariables);
		String sistemaProviniente = extraerSistemaProviniente(hojaDeVariables);
		List<String> camposNoKey = extraerCamposNoKey(hojaDeVariables);
		List<String> camposKey = extraerCamposKey(hojaDeVariables);
		workbookExcelDatos.close();
		// preparo el txt

		notasService.createFirstStarProcedure(nombreTabla,sistemaProviniente,camposNoKey,camposKey);
		notasService.createSecondStarProcedure(nombreTabla,sistemaProviniente,camposNoKey,camposKey);
		notasService.createThirdStarProcedure(nombreTabla,sistemaProviniente,camposNoKey,camposKey);
		notasService.createFourthStarProcedure(nombreTabla,sistemaProviniente,camposNoKey,camposKey);
		

		

		return ResponseEntity.ok().body("creado");
	}


//	private void cargarDatosvariables(XSSFSheet sheetVariables) {
//		int rowNum = 0;
//		Row row = sheetVariables.createRow(rowNum++);
//		int colNum = 0;
//		Cell cell = row.createCell(colNum++);
//		cell.setCellValue("NOMBRE TABLA ");
//		cell = row.createCell(colNum++);
//		cell.setCellValue("NOMBRE SISTEMA");
//		cell = row.createCell(colNum++);
//		cell.setCellValue("CAMPO KEY");
//		cell = row.createCell(colNum++);
//		cell.setCellValue("CAMPO NO KEY ");
//		for (int x = 0; x < 10; x++) {
//			colNum = 0;
//			row = sheetVariables.createRow(rowNum++);
//			cell = row.createCell(colNum++);
//			cell.setCellValue("NOMBRE TABLA" + x);
//			cell = row.createCell(colNum++);
//			cell.setCellValue("NOMBRE SISTEMA" + x);
//			cell = row.createCell(colNum++);
//			cell.setCellValue("CAMPO KEY" + x);
//			cell = row.createCell(colNum++);
//			cell.setCellValue("CAMPO NO KEY" + x);
//		}
//
//	}

	private String extraerNombreTabla(Sheet hojaDeVariables) {
		Iterator<Row> filaHojaDeVariables = hojaDeVariables.iterator();
		if (filaHojaDeVariables.hasNext()) {
			filaHojaDeVariables.next();
		}
		if (filaHojaDeVariables.hasNext()) {

			Row currentRow = filaHojaDeVariables.next();
			return currentRow.getCell(1).getStringCellValue();
		}
		return "No se encontro el nombre de la tabla";
	}

	private String extraerSistemaProviniente(Sheet hojaDeVariables) {
		Iterator<Row> filaHojaDeVariables = hojaDeVariables.iterator();
		if (filaHojaDeVariables.hasNext()) {
			filaHojaDeVariables.next();
		}
		if (filaHojaDeVariables.hasNext()) {

			Row currentRow = filaHojaDeVariables.next();
			return currentRow.getCell(2).getStringCellValue();
		}
		return "No se encontro el sistema proviniente";
	}

	private List<String> extraerCamposKey(Sheet hojaDeVariables) {
		Iterator<Row> filaHojaDeVariables = hojaDeVariables.iterator();
		filaHojaDeVariables.next();
		List<String> camposKey = new ArrayList<>();
		while (filaHojaDeVariables.hasNext()) {

			Row currentRow = filaHojaDeVariables.next();
			if(currentRow.getCell(3)!=null && currentRow.getCell(3).getStringCellValue()!=null&&currentRow.getCell(3).getStringCellValue()!="")
			{
			camposKey.add(currentRow.getCell(3).getStringCellValue());
			}
		}
		return camposKey;
	}

	private List<String> extraerCamposNoKey(Sheet hojaDeVariables) {
		Iterator<Row> filaHojaDeVariables = hojaDeVariables.iterator();
		filaHojaDeVariables.next();
		List<String> camposNoKey = new ArrayList<>();
		while (filaHojaDeVariables.hasNext()) {

			Row currentRow = filaHojaDeVariables.next();
			if(currentRow.getCell(4)!=null && currentRow.getCell(4).getStringCellValue()!=null&&currentRow.getCell(4).getStringCellValue()!="")
			{
			camposNoKey.add(currentRow.getCell(4).getStringCellValue());
			}
		}
		return camposNoKey;
	}

}
