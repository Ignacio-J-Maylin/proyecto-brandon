package com.brandonblocknotas.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class NotasService {

	public void createFirstStarProcedure(String nombreTabla, String sistemaProviniente, List<String> camposNoKey,
			List<String> camposKey) {
		File file = new File(nombreTabla+" PrimerSTOREDPROCEDURE.txt");

		try (Writer writer = new BufferedWriter(new FileWriter(file))) {
			String contents = "-- 1.CONSOLIDACIÓN_Carga a STG Tablas Derivadas " + System.getProperty("line.separator")
					+ "-- Carga  cada Tabla temporales Replica Derivada, discriminado por: operation_flag = 'L'  ;'I'; 'U'; 'D'"
					+ System.getProperty("line.separator")
					+ "--*******************************************************************************************"
					+ System.getProperty("line.separator")
					+ "-- INICIO Carga Tabla Derivada '_I': wc_v_funding_rate_mes wc_v_consol_mes_cerrado "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + " CREATE OR REPLACE TABLE meli-bi-data.SAP_STG.1p_"
					+ nombreTabla + "_L" + System.getProperty("line.separator") + " AS "
					+ System.getProperty("line.separator") + " SELECT S.* FROM " + System.getProperty("line.separator")
					+ " (SELECT DISTINCT " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator")
					+ String.join("," + System.getProperty("line.separator"), camposNoKey) + ","
					+ System.getProperty("line.separator") + " operation_flag," + System.getProperty("line.separator")
					+ " is_deleted," + System.getProperty("line.separator") + " recordstamp"
					+ System.getProperty("line.separator") + " FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + " WHERE operation_flag = 'L'"
					+ System.getProperty("line.separator") + " ) S" + System.getProperty("line.separator")
					+ " INNER JOIN" + System.getProperty("line.separator") + " (SELECT DISTINCT"
					+ System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator") + " MAX(recordstamp)            recordstamp"
					+ System.getProperty("line.separator") + "  FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + " WHERE operation_flag = 'L'  "
					+ System.getProperty("line.separator") + " GROUP BY " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + "--key"
					+ System.getProperty("line.separator") + " ) F " + System.getProperty("line.separator")
					+ onIfNull(camposKey) + System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "-- FIN Carga Tabla Derivada '_L': "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "-- iNICIO Carga Tabla Derivada '_I': "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + " CREATE OR REPLACE TABLE meli-bi-data.SAP_STG.1p_"
					+ nombreTabla + "_I" + System.getProperty("line.separator") + " AS "
					+ System.getProperty("line.separator") + " SELECT S.* FROM" + System.getProperty("line.separator")
					+ " (SELECT DISTINCT " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator")
					+ String.join("," + System.getProperty("line.separator"), camposNoKey) + ","
					+ System.getProperty("line.separator") + " operation_flag," + System.getProperty("line.separator")
					+ " is_deleted," + System.getProperty("line.separator") + " recordstamp"
					+ System.getProperty("line.separator") + " FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + " WHERE operation_flag = 'I'"
					+ System.getProperty("line.separator") + " ) S " + System.getProperty("line.separator")
					+ " INNER JOIN" + System.getProperty("line.separator") + " (SELECT DISTINCT "
					+ System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator") + " MAX(recordstamp)            recordstamp"
					+ System.getProperty("line.separator") + "  FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + "  WHERE operation_flag = 'I'  "
					+ System.getProperty("line.separator") + "  GROUP BY " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + "--key"
					+ System.getProperty("line.separator") + "  ) F " + System.getProperty("line.separator")
					+ onIfNull(camposKey) + System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "-- FIN Carga Tabla Derivada '_I': "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "-- iNICIO Carga Tabla Derivada '_U': "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + " CREATE OR REPLACE TABLE meli-bi-data.SAP_STG.1p_"
					+ nombreTabla + "_U" + System.getProperty("line.separator") + " AS"
					+ System.getProperty("line.separator") + " SELECT S.* FROM" + System.getProperty("line.separator")
					+ " (SELECT DISTINCT " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator")
					+ String.join("," + System.getProperty("line.separator"), camposNoKey) + ","
					+ System.getProperty("line.separator") + " operation_flag," + System.getProperty("line.separator")
					+ " is_deleted," + System.getProperty("line.separator") + " recordstamp"
					+ System.getProperty("line.separator") + " FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + " WHERE operation_flag = 'U'"
					+ System.getProperty("line.separator") + " ) S " + System.getProperty("line.separator")
					+ " INNER JOIN " + System.getProperty("line.separator") + " (SELECT DISTINCT"
					+ System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator") + " MAX(recordstamp)            recordstamp"
					+ System.getProperty("line.separator") + "  FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + "  WHERE operation_flag = 'U'  "
					+ System.getProperty("line.separator") + "  GROUP BY " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + "--key"
					+ System.getProperty("line.separator") + " ) F " + System.getProperty("line.separator")
					+ onIfNull(camposKey) + System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "-- FIN Carga Tabla Derivada '_U': "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "-- iNICIO Carga Tabla Derivada '_D': "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "CREATE OR REPLACE TABLE meli-bi-data.SAP_STG.1p_"
					+ nombreTabla + "_D" + System.getProperty("line.separator") + " AS "
					+ System.getProperty("line.separator") + " SELECT S.* FROM " + System.getProperty("line.separator")
					+ " (SELECT DISTINCT " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator")
					+ String.join("," + System.getProperty("line.separator"), camposNoKey) + ","
					+ System.getProperty("line.separator") + " operation_flag," + System.getProperty("line.separator")
					+ " is_deleted," + System.getProperty("line.separator") + " recordstamp"
					+ System.getProperty("line.separator") + " FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + " WHERE operation_flag = 'D'"
					+ System.getProperty("line.separator") + " ) S " + System.getProperty("line.separator")
					+ " INNER JOIN " + System.getProperty("line.separator") + " (SELECT DISTINCT"
					+ System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + ",--key"
					+ System.getProperty("line.separator") + " MAX(recordstamp)            recordstamp"
					+ System.getProperty("line.separator") + "  FROM meli-bi-data.SAP_" + sistemaProviniente + "_FEED."
					+ nombreTabla + System.getProperty("line.separator") + "  WHERE operation_flag = 'D'  "
					+ System.getProperty("line.separator") + "  GROUP BY " + System.getProperty("line.separator")
					+ String.join(",--key" + System.getProperty("line.separator"), camposKey) + "--key"
					+ System.getProperty("line.separator") + " ) F " + System.getProperty("line.separator")
					+ onIfNull(camposKey)+ ";" + System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator") + "-- FIN Carga Tabla Derivada '_D': "
					+ System.getProperty("line.separator")
					+ "-- *******************************************************************************************"
					+ System.getProperty("line.separator");

			writer.write(contents);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createSecondStarProcedure(String tabla, String sistema, List<String> noKey,
			List<String> key) {
		File file = new File(tabla+" SegundoSTOREDPROCEDURE.txt");

		try (Writer writer = new BufferedWriter(new FileWriter(file))) {
			String contents = "-- 2.CONSOLIDACIÓN_Union tabla _Ly_I "
		    + System.getProperty("line.separator")
			+ "-- Se Unen y Eliminan duplicados de la tablas '_L' y '_I'."
			+ System.getProperty("line.separator")
			+ "CREATE OR REPLACE TABLE meli-bi-data.SAP_STG.1p_"+tabla+"_LI"
			+ System.getProperty("line.separator")
			+ " AS "
			+ System.getProperty("line.separator")
			+ "SELECT S.* FROM"
			+ System.getProperty("line.separator")
			+ " (SELECT DISTINCT "
			+ System.getProperty("line.separator")
			+ String.join(",--key" + System.getProperty("line.separator"), key) + ",--key"
			+ System.getProperty("line.separator")
			+ String.join("," + System.getProperty("line.separator"), noKey) + ","
			+ System.getProperty("line.separator") + " operation_flag," + System.getProperty("line.separator")
			+ " is_deleted," + System.getProperty("line.separator") + " recordstamp"
			+ System.getProperty("line.separator")
			+ " FROM "
			+ System.getProperty("line.separator")
			+ " ( "
			+ System.getProperty("line.separator")
			+ "   SELECT * FROM meli-bi-data.SAP_STG.1p_"+tabla+"_L"
			+ System.getProperty("line.separator")
			+ "  UNION ALL "
			+ System.getProperty("line.separator")
			+ "   SELECT * FROM meli-bi-data.SAP_STG.1p_"+tabla+"_I"
			+ System.getProperty("line.separator")
			+ " ) "
			+ System.getProperty("line.separator")
			+ " ) S "
			+ System.getProperty("line.separator")
			+ " INNER JOIN "
			+ System.getProperty("line.separator")
			+ " (SELECT DISTINCT "
			+ System.getProperty("line.separator")
			+ String.join(",--key" + System.getProperty("line.separator"), key) + ",--key"
			+ System.getProperty("line.separator") + " MAX(recordstamp)            recordstamp"
			+ System.getProperty("line.separator")
			+ "  FROM "
			+ System.getProperty("line.separator")
			+ "   ( "
			+ System.getProperty("line.separator")
			+ "   SELECT * FROM meli-bi-data.SAP_STG.1p_"+tabla+"_L"
			+ System.getProperty("line.separator")
			+ "   UNION ALL "
			+ System.getProperty("line.separator")
			+ "   SELECT * FROM meli-bi-data.SAP_STG.1p_"+tabla+"_I "
			+ System.getProperty("line.separator")
			+ "   ) "
			+ System.getProperty("line.separator")
			+ "  GROUP BY  "
			+ System.getProperty("line.separator")
			+ String.join(",--key" + System.getProperty("line.separator"), key) + "--key"
			+ System.getProperty("line.separator") + " ) F " + System.getProperty("line.separator")
			+ onIfNull(key) 
			+ ";";
			writer.write(contents);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createThirdStarProcedure(String tabla, String sistema, List<String> noKey,
			List<String> key) {
		File file = new File(tabla+" TercerSTOREDPROCEDURE.txt");

		try (Writer writer = new BufferedWriter(new FileWriter(file))) {
			String contents = 
					"-- 3.CONSOLIDACIÓN_Update tabla _LI "
			+ System.getProperty("line.separator")
			+ "-- Actualiza tabla '_LI' desde '_U'."
			+ System.getProperty("line.separator")
			+ "UPDATE meli-bi-data.SAP_STG.1p_"+tabla+"_LI LI"
			+ System.getProperty("line.separator")
			+ "     SET "
			+ System.getProperty("line.separator")
			+ liNoKeyNumSameUNoKeyNum(noKey)
			+ System.getProperty("line.separator")
			+ "--no son key-- "
			+ System.getProperty("line.separator")
			+ "FROM meli-bi-data.SAP_STG.1p_"+tabla+"_U U"
			+ System.getProperty("line.separator")
			+ " WHERE "
			+ System.getProperty("line.separator")
			+ ifNollLiwSameIfNullUKey(key)
			+ ";"
			+ System.getProperty("line.separator")
			+ "--son key--"
			+ ""
			+ System.getProperty("line.separator")
			+ ""
			+ System.getProperty("line.separator")
			+ ""
			+ System.getProperty("line.separator")
			+ ""
			+ System.getProperty("line.separator")
			+ ""
			+ System.getProperty("line.separator");

			writer.write(contents);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


	public void createFourthStarProcedure(String tabla, String sistema , List<String> noKey,
			List<String> key) {
		File file = new File(tabla+" CuartoSTOREDPROCEDURE.txt");

		try (Writer writer = new BufferedWriter(new FileWriter(file))) {
			String contents = 
			"-- INICIO Borrado de '_LI' los '_D': "
			+ System.getProperty("line.separator")
			+ "-- *******************************************************************************************"
			+ System.getProperty("line.separator")
			+ " MERGE INTO meli-bi-data.SAP_STG.1p_"+tabla+"_LI D"
			+ System.getProperty("line.separator")
			+ " USING "
			+ System.getProperty("line.separator")
			+ " (SELECT DISTINCT "
			+ System.getProperty("line.separator")
			+ String.join(",--key" + System.getProperty("line.separator"), key) + "--key"
			+ System.getProperty("line.separator")
			+ "   FROM meli-bi-data.SAP_STG.1p_"+tabla+"_D "
			+ System.getProperty("line.separator")
			+ "       ) O    "
			+ System.getProperty("line.separator")
			+ " -- Condicion Merge  "
			+ System.getProperty("line.separator")
			+ onIfNullDKeyNumSameIfNullOKey(key)
			+ System.getProperty("line.separator")
			+ " WHEN MATCHED THEN "
			+ System.getProperty("line.separator")
			+ "   DELETE ;"
			+ System.getProperty("line.separator")
			+ " -- *******************************************************************************************"
			+ System.getProperty("line.separator")
			+ " -- FIN Borrado de '_LI' los '_D':"
			+ System.getProperty("line.separator")
			+ "-- *******************************************************************************************";

			writer.write(contents);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private String onIfNull(List<String> camposKey) {
		StringBuffer renglon = new StringBuffer();
		renglon.append(" ON    IFNULL(S." + camposKey.get(0) + "  ,'')  = IFNULL(F." + camposKey.get(0) + ",'') ");
		renglon.append(System.getProperty("line.separator"));
		camposKey.subList(1, camposKey.size()).forEach(
				c -> renglon.append(" AND IFNULL(S." + c + "         ,'')  = IFNULL(F." + c + "         ,'')"+System.getProperty("line.separator")));
		renglon.append(" AND S.recordstamp = F.recordstamp ");
		return renglon.toString();
	}
	private String liNoKeyNumSameUNoKeyNum(List<String> noKey) {
		StringBuffer renglon = new StringBuffer();
		noKey.forEach(
				c -> renglon.append(System.getProperty("line.separator")+"LI."+c+"= U."+c+"," ));
		return renglon.toString().substring(0, renglon.toString().length()-1);
	}
	private String ifNollLiwSameIfNullUKey(List<String> key) {
		StringBuffer renglon = new StringBuffer();
		key.forEach(
				c -> renglon.append(System.getProperty("line.separator")+
						"IFNULL(LI."+c+"          ,'')  = IFNULL(U."+c+"           ,'') AND"));
		return renglon.toString().substring(0, renglon.toString().length()-3);
	}
	

	private String onIfNullDKeyNumSameIfNullOKey(List<String> key) {
		StringBuffer renglon = new StringBuffer();
		renglon.append(" ON  (  IFNULL(D." + key.get(0) + "  ,'')  = IFNULL(O." + key.get(0) + ",'') ");
		renglon.append(System.getProperty("line.separator"));
		key.subList(1, key.size()).forEach(
				c -> renglon.append(" AND IFNULL(D." + c + "         ,'')  = IFNULL(O." + c + "         ,'')"+System.getProperty("line.separator")));
		renglon.append(" )");
		return renglon.toString();
	}
}
