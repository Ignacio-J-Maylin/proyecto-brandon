package com.brandonblocknotas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrandonBlockNotasApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrandonBlockNotasApplication.class, args);
	}

}
